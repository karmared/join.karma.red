const ChatScript = `
  <!-- Chat {literal} -->
<script type="text/javascript" encoding="utf-8">
var _tbEmbedArgs = _tbEmbedArgs || [];
(function () {
  var u =  "https://tb-kube-loadbalancer-prod.textback.io/tb-widget-srv/widget";
  _tbEmbedArgs.push(["widgetId", "5cf5fceb-3cd6-4bee-b72e-fda204f85b30"]);
  _tbEmbedArgs.push(["baseUrl", u]);

  var d = document, g = d.createElement("script"), s = d.getElementsByTagName("script")[0];
  g.type = "text/javascript";
  g.charset = "utf-8";
  g.defer = true;
  g.async = true;
  g.src = u + "/widget.js";
  s.parentNode.insertBefore(g, s);
})();
</script>
<!-- /Chat {/literal} -->
`

const Roistat = `
<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', 'f9015fe177da033f2c8d26da8f91e4f5');
</script>
`

const AmoPixel = `
  <!-- Amo Pixel -->
  <script type="text/javascript">(function (w, d) {w.amo_pixel_token = 'p1Ip4iB8yqaEyXU9HPmcTNFk190/n6qSlHVan6YCmIVcipMPIyQEQxW2+R3DID6e';var s = document.createElement('script'), f = d.getElementsByTagName('script')[0];s.id = 'amo_pixel_js';s.type = 'text/javascript';s.async = true;s.src = 'https://piper.amocrm.ru/pixel/js/tracker/pixel.js?token=' + w.amo_pixel_token;f.parentNode.insertBefore(s, f);})(window, document);</script>
  <!-- /Amo Pixel -->
`

const GoogleAnalytics = `
<!-- Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127969433-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-127969433-1');
</script>
<!-- /Google Analytics -->
`

const GTAGManagerNoScript = `
<!-- Google Tag Manager (noscript) -->
<noscript>
  <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WDKH3K7" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
`

const GANKarma = `
<script>
(function(w,d,s,l,i){
  w[l]= w[l]||[];

  w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});

  var f = d.getElementsByTagName(s)[0],
      j = d.createElement(s),
      dl = l !='dataLayer'?'&l='+l:'';

      j.async=true;
      j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;
      f.parentNode.insertBefore(j,f);
      
})(window,document,'script','dataLayer','GTM-WDKH3K7');</script>
`

module.exports = {
  Roistat,
  AmoPixel,
  GANKarma,
  ChatScript,
  GoogleAnalytics,
  GTAGManagerNoScript,
}
