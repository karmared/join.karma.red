const path = require("path")

module.exports = {
  config: path.resolve(process.cwd(), "config"),
  schema: path.resolve(process.cwd(), "config", "schema.graphql"),
}
