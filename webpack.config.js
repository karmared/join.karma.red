/* eslint-disable */
require("./config/env")

const path = require("path")
const webpack = require("webpack")
const CopyWebpackPlugin = require("copy-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")

const {
  Roistat,
  AmoPixel,
  GANKarma,
  ChatScript,
  GoogleAnalytics,
  GTAGManagerNoScript,
} = require("./config/templateInjections")

module.exports = () => {
  return {
    mode: process.env.NODE_ENV,
    entry: {
      main: "./src/index.js",
    },
    output: {
      path: path.resolve(__dirname, "public"),
      filename: "[name]~[hash]~bundle.js",
      publicPath: "/",
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader",
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: ["eslint-loader"],
        },
      ],
    },
    devServer: {
      open: true,
      port: process.env.PORT,
      historyApiFallback: true,
      disableHostCheck: true
    },
    optimization: {
      splitChunks: {
        chunks: "all",
        minSize: 0,
      },
      runtimeChunk: true,
      concatenateModules: true,
    },
    plugins: [
      new webpack.EnvironmentPlugin([
        "NODE_ENV",
        "GRAPHQL_ENDPOINT",
      ]),
      new HtmlWebpackPlugin({
        filename: path.resolve(__dirname, "public", "index.html"),
        template: "src/index.html",
        Roistat,
        GANKarma,
        AmoPixel,
        ChatScript,
        GoogleAnalytics,
        GTAGManagerNoScript,
      }),
      new webpack.HashedModuleIdsPlugin({
        hashDigestLength: 4,
      }),
      new CopyWebpackPlugin([
        {
          from: path.resolve(__dirname, "static"),
          to: path.resolve(__dirname, "public"),
        },
      ]),
    ],
  }
}
