require("./config/env")
const convert = require('koa-connect')
const historyApiFallback = require('connect-history-api-fallback')
const webpackServeWaitpage = require("webpack-serve-waitpage")


module.exports = {
  port: process.env.PORT,
  add: (app, middleware, options) => {
    app.use(webpackServeWaitpage(options))
    app.use(convert(historyApiFallback({})))
  }
}
