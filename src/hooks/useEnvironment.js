import { useContext } from "react"
import { RelayEnvironmentContext } from "/context"

const useEnvironment = () => useContext(RelayEnvironmentContext)

export default useEnvironment
