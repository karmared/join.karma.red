import React from "react"

import { graphql } from "react-relay"
import { QueryRenderer } from "/components"

const query = graphql`
  query OrderInfoQuery($id: ID!) {
    node(id: $id) {
      id
      __typename
      ... on Order {
        status
      }
    }
  }
`

const container = render => ({ orderId, ...rest }) => {
  const variables = {
    id: orderId,
  }

  return (
    <QueryRenderer
      {...rest}
      query={query}
      render={render}
      variables={variables}
    />
  )
}

container.query = query

export default container
