/* global window */
import React from "react"

import i18n from "/i18n"

const LocaleContext = React.createContext()

const storage = {
  set: locale => window.localStorage.setItem("locale", locale),
  get: defaultLocale => window.localStorage.getItem("locale") || defaultLocale,
}

export const LocaleConsumer = LocaleContext.Consumer

export class LocaleProvider extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      locale: null,
      translate: (...args) => i18n.t(...args),
      setLocale: this.setLocale,
    }

    i18n.init({
      lng: storage.get("ru"),
      fallbackLng: "en",
      ns: ["common", "pages", "components", "errors"],
      defaultNS: "common",
    }, this.setLocaleState)
  }

  setLocale = (locale) => {
    if (i18n.language === locale) return

    storage.set(locale)
    i18n.changeLanguage(locale, this.setLocaleState)
  }

  setLocaleState = () => {
    const locale = i18n.language || i18n.options.fallbackLng[0]
    this.setState({ locale })
  }

  render() {
    if (this.state.locale === null) return null

    return (
      <LocaleContext.Provider value={this.state}>
        {this.props.children}
      </LocaleContext.Provider>
    )
  }
}
