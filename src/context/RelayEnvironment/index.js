import React from "react"
import createEnvironment from "/environment"

const RelayEnvironmentContext = React.createContext()

const RelayEnvironmentConsumer = RelayEnvironmentContext.Consumer

class RelayEnvironmentProvider extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      environment: createEnvironment(),
      refreshEnvironment: this.refreshEnvironment,
    }
  }

  refreshEnvironment = () => {
    const environment = createEnvironment()
    this.setState(() => ({ environment }))
  }

  render() {
    return (
      <RelayEnvironmentContext.Provider value={this.state}>
        {this.props.children}
      </RelayEnvironmentContext.Provider>
    )
  }
}

export {
  RelayEnvironmentContext,
  RelayEnvironmentConsumer,
  RelayEnvironmentProvider,
}
