import i18next from "i18next"
import backend from "./backend"

i18next.use(backend)

export default i18next
