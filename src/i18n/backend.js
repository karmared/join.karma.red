const locales = {
  aa: () => import(/* webpackChunkName: "locale-aa" */ "/locales/aa.json"),
  en: () => import(/* webpackChunkName: "locale-en" */ "/locales/en.json"),
  ja: () => import(/* webpackChunkName: "locale-ja" */ "/locales/ja.json"),
  ko: () => import(/* webpackChunkName: "locale-ko" */ "/locales/ko.json"),
  ru: () => import(/* webpackChunkName: "locale-ru" */ "/locales/ru.json"),
  zh: () => import(/* webpackChunkName: "locale-zh" */ "/locales/zh.json"),
}

/* global window */
/* eslint-disable consistent-return */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-param-reassign */

export default {
  type: "backend",
  read: (language, namespace, callback) => {
    if (window._jipt) language = "aa"

    if (typeof locales[language] !== "function") return callback(null, {})

    locales[language]().then((data) => {
      callback(null, data[namespace])
    })
  },
}
