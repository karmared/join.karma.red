/* global fetch window FormData */
import "whatwg-fetch"
import { Observable } from "rxjs"

const getBearerToken = () => `Bearer ${window.localStorage.getItem("karma-auth-token")}`

const fetchQuery = (operation, variables) => fetch(process.env.GRAPHQL_ENDPOINT, {
  method: "POST",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: getBearerToken(),
  },
  body: JSON.stringify({
    query: operation.text,
    variables,
  }),
}).then(response => response.json())


const fetchMutation = (operation, variables, uploadables) => {
  const formData = new FormData()

  formData.append("query", operation.text)
  formData.append("variables", JSON.stringify(variables))

  Object.keys(uploadables || {}).forEach((name) => {
    formData.append(name, uploadables[name])
  })

  return fetch(process.env.GRAPHQL_ENDPOINT, {
    method: "POST",
    headers: {
      Accept: "application/json",
      Authorization: getBearerToken(),
    },
    body: formData,
  }).then(response => response.json())
}


const fetchQuery$ = (operation, variables, cacheConfig) => Observable.create((observer) => {
  fetchQuery(operation, variables, cacheConfig).then((json) => {
    if (json.errors) observer.error(json)
    observer.next(json)
    observer.complete()
  })
})


const fetchMutation$ = (operation, variables, uploadables) => Observable.create((observer) => {
  fetchMutation(operation, variables, uploadables).then((json) => {
    if (json.errors) observer.error(json)
    observer.next(json)
    observer.complete()
  })
})


const fetchQueryOrMutation = (
  operation,
  variables,
  cacheConfig,
  uploadables,
) => {
  switch (operation.operationKind) {
    case "query":
      return fetchQuery$(operation, variables, cacheConfig)
    case "mutation":
      return fetchMutation$(operation, variables, uploadables)
    default:
      throw new Error(`Unknown operation "${operation.operationKind}"`)
  }
}


export default fetchQueryOrMutation
