import {
  Store,
  Network,
  Environment,
  RecordSource,
} from "relay-runtime"
import fetch from "./fetch"

const createStore = () => new Store(new RecordSource())

const createNetwork = () => Network.create(fetch)

export default () => new Environment({
  store: createStore(),
  network: createNetwork(),
})
