import enhance from "/mutations/enhance"

/* global graphql */
const mutation = graphql`
  mutation ConfirmPhoneMutation(
    $input: ConfirmPhoneInput!
  ) {
    confirmPhone(input: $input) {
      signedPhone
    }
  }

`

export default enhance({ mutation })
