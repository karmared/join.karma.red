import enhance from "/mutations/enhance"

/* global graphql */
const mutation = graphql`
  mutation RegisterUserMutation(
    $input: RegisterUserInput!
  ) {
    registerUser(input: $input) {
      status
    }
  }
`

export default enhance({ mutation })
