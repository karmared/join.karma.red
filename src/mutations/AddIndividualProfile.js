import enhance from "/mutations/enhance"

/* global graphql */
const mutation = graphql`
  mutation AddIndividualProfileMutation(
    $input: AddIndividualProfileInput!
  ) {
    addIndividualProfile(input: $input) {
      profile {
        id
        name
        phone
      }
    }
  }
`

const updater = (store) => {
  const root = store.getRoot()
  const viewer = root.getLinkedRecord("viewer")
  const profiles = viewer.getLinkedRecords("profiles") || []
  const payload = store.getRootField("addIndividualProfile")
  const profile = payload.getLinkedRecord("profile")

  viewer.setLinkedRecords([...profiles, profile], "profiles")
}

export default enhance({ mutation, updater })
