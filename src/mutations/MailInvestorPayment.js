import enhance from "/mutations/enhance"

/* global graphql */
const mutation = graphql`
  mutation MailInvestorPaymentMutation (
    $input: MailInvestorPaymentInput!
  ) {
    mailInvestorPayment(input: $input) {
      status
    }
  }
`

export default enhance({ mutation })
