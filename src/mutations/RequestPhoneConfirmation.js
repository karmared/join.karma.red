import enhance from "/mutations/enhance"

/* global graphql */
const mutation = graphql`
  mutation RequestPhoneConfirmationMutation(
    $input: RequestPhoneConfirmationInput!
  ) {
    requestPhoneConfirmation(input: $input) {
      status
    }
  }
`

export default enhance({ mutation })
