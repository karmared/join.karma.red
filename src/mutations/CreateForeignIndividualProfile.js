import enhance from "/mutations/enhance"

/* global graphql */
const mutation = graphql`
  mutation CreateForeignIndividualProfileMutation(
    $input: CreateForeignIndividualProfileInput!
  ) {
    createForeignIndividualProfile(input: $input) {
      profile {
        id
        __typename
        phone
        firstName
        lastName
      }
    }
  }
`

const updater = (store) => {
  const root = store.getRoot()
  const viewer = root.getLinkedRecord("viewer")
  const foreignProfiles = viewer.getLinkedRecords("foreignProfiles") || []
  const payload = store.getRootField("createForeignIndividualProfile")
  const profile = payload.getLinkedRecord("profile")

  viewer.setLinkedRecords([...foreignProfiles, profile], "foreignProfiles")
}

export default enhance({ mutation, updater })
