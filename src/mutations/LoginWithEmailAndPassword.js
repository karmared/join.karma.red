import enhance from "/mutations/enhance"

/* global graphql */
const mutation = graphql`
  mutation LoginWithEmailAndPasswordMutation(
    $input: LoginWithEmailAndPasswordInput!
  ) {
    loginWithEmailAndPassword(input: $input) {
      token
    }
  }
`


export default enhance({ mutation })
