/* eslint-disable no-unused-vars */
/* eslint-disable import/prefer-default-export */

import React from "react"
import Dynamic from "/components/Dynamic"

export const dynamic = load => props => <Dynamic {...props} load={load} />
