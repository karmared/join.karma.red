import { createGlobalStyle } from "styled-components"

import Fonts from "./fonts"

/* eslint-disable  no-unused-expressions */
const GlobalStyle = createGlobalStyle`
  html {
    box-sizing: border-box;
    font-family: "Georgia", Georgia, sans-serif;
    font-size: 16px;
    height: 100%;
  }

  body {
    height: 100%;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
  }

  *,
  *::after,
  *::before {
    box-sizing: inherit;
    font-family: inherit;
    outline: none;
  }
  
  button {
    display: inline-block;
    border: none;
    padding: 1rem 2rem;
    margin: 0;
    text-decoration: none;
    background: none;
    font-family: sans-serif;
    font-size: 1rem;
    cursor: pointer;
    text-align: center;
    transition: background 250ms ease-in-out, 
                transform 150ms ease;
    -webkit-appearance: none;
    -moz-appearance: none;
    outline-color: rgb(0, 103, 244);
  }


  a[data-current] {
    color: currentColor;
  }

  a[data-no-underline] {
    text-decoration: none;
  }


  #app-root {
    height: 100%;
    position: relative;
  }
`

GlobalStyle.fonts = Fonts
export default GlobalStyle
