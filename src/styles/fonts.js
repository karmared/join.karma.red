import { css, createGlobalStyle } from "styled-components"

const fonts = [
  {
    name: "Regular",
    weight: 400,
  },
  {
    name: "Medium",
    weight: 500,
  },
  {
    name: "Bold",
    weight: 600,
  },
]

const fontFaces = fonts.map(font => css`
    @font-face {
      font-family: "Georgia";
      font-weight: ${font.weight};
    }
  `)

/* eslint-disable  no-unused-expressions */
const Fonts = createGlobalStyle`
  ${fontFaces}
`

export default Fonts
