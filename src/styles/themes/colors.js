const steps = [
  0.0,
  0.1,
  0.2,
  0.3,
  0.4,
  0.5,
  0.6,
  0.7,
  0.8,
  0.9,
  1.0,
]

const Color = (name, parts) => {
  const plural = `${name}s`

  return {
    [name]: `rgb(${parts.join(",")})`,
    [plural]: steps.map(opacity => `rgba(${parts.join(",")},${opacity})`),
  }
}

export default {
  ...Color("red", [247, 0, 0]),
  ...Color("blue", [29, 78, 240]),

  // ...Color("greyDarkest", [53, 58, 67]),
  // ...Color("greyDarker", [123, 131, 139]),
  ...Color("greyDark", [218, 218, 218]),
  ...Color("greyLight", [250, 250, 250]),
  // ...Color("greyLightest", [71, 77, 88]),

  ...Color("greyShuttle", [74, 74, 74]),

  // ...Color("grey", [217, 222, 226]),

  ...Color("black", [11, 16, 22]),
  ...Color("white", [255, 255, 255]),
}
