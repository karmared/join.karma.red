import colors from "./colors"

export default {
  default: {
    height: "50px",
    padding: "0 75px",
    border: "none",
    borderRadius: "5px",
    fontFamily: "Arial",
    fontWeight: "700",
    color: colors.white,
    textTransform: "uppercase",
    backgroundColor: colors.red,
    letterSpacing: "1.5px",

    "&::disabled": {
      opacity: "0.5",
    },
  },
}
