import React from "react"

class Dynamic extends React.Component {
  state = {
    component: null,
  }

  componentDidMount() {
    this.props.load().then((module) => {
      this.setState(() => ({ component: module.default || module }))
    })
  }

  render() {
    if (this.state.component === null) return null

    return (
      <this.state.component {...this.props} />
    )
  }
}

export default Dynamic
