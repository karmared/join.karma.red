/* global window */
import React from "react"

import { setUtmContent } from "/utils"

const UtmWatcher = () => {
  React.useEffect(() => setUtmContent(), [])

  return null
}

export default UtmWatcher
