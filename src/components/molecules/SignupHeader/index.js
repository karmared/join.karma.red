import React from "react"

import { Box, Flex } from "/components"

import {
  Logo,
  Header,
  Divider,
  LogoText,
  Assistant,
  StyledLink,
  HelpContainer,
} from "./styles"

export default () => (
  <Header>
    <Flex
      my="10px"
      px="40px"
      alignItems="center"
      justifyContent="space-between"
    >
      <StyledLink href="https://my.karma.red" target="_blank">
        <Flex alignItems="center">
          <Box is={Logo} mr="5px"/>
          <LogoText>
            Karma
          </LogoText>
        </Flex>
      </StyledLink>
      <HelpContainer href="https://t.me/karmainvest" target="_blank">
        ?
      </HelpContainer>
    </Flex>
    <Divider/>
    <Assistant/>
  </Header>
)
