import styled from "styled-components"

import { Box, Text } from "/components"

export const Header = styled.header`
  position: relative;
  margin-bottom: 35px;
`

export const Divider = styled(Box)`
  width: 100%;
  height: 1px;
  background-color: ${({ theme }) => theme.colors.greyDark};
`

export const Logo = styled.div`
  width: 34px;
  height: 34px;
  border-radius: 50%;
  background-image: url("/images/logo.png");
  background-position: center center;
  background-size: cover;
`

export const LogoText = styled(Text)`
  font-size: 20px;
  font-family: "Open Sans";
  font-weight: 700;
  
  @media (max-width: 479px) {
    display: none;
  }
`

export const StyledLink = styled.a`
  opacity: 0.5;
  text-decoration: none;
  cursor: pointer;
  
  &:active, &:visited {
    color: black;
  }
`

export const HelpContainer = styled.a`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  border: 1px solid black;
  cursor: pointer;
  font-family: "Open Sans";
  
  text-decoration: none;
  
  &:active, &:visited {
    color: black;
  }
`

export const Assistant = styled(Box)`
  width: 70px;
  height: 70px;
  border-radius: 50%;
  background-image: url("/images/assistant.jpg");
  background-position: center center;
  background-size: cover;
  
  position: absolute;
  left: calc(50% - 35px);
  bottom: -35px;
`
