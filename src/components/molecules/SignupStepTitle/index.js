import React from "react"
import styled from "styled-components"

import { Text } from "/components"

const StyledText = styled(Text)`
  word-break: break-word;
`

export default ({ children, ...props }) => (
  <StyledText
    is="h2"
    fontSize="30px"
    textAlign="center"
    color="greyShuttle"
    fontWeight="500"
    {...props}
  >
    {children}
  </StyledText>
)
