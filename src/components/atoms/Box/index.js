import React from "react"
import styled from "styled-components"

import {
  flex,
  color,
  order,
  space,
  width,
  height,
  display,
  borders,
  fontSize,
  alignSelf,
  lineHeight,
  borderColor,
  borderRadius,
} from "styled-system"


const Box = styled.div`
  ${display}
  ${flex}
  ${color}
  ${order}
  ${space}
  ${width}
  ${height}
  ${borders}
  ${fontSize}
  ${alignSelf}
  ${lineHeight}
  ${borderColor}
  ${borderRadius}
`


const render = ({ is, ...props }) => {
  const Component = is ? Box.withComponent(is) : Box
  return <Component {...props} />
}


export default render
