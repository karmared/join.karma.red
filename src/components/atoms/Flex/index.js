import React from "react"
import styled from "styled-components"
import {
  width,
  flexWrap,
  alignItems,
  flexDirection,
  justifyContent,
} from "styled-system"
import Box from "../Box"


const Flex = styled(Box)`
  display: flex;
  ${width}
  ${alignItems}
  ${flexWrap}
  ${flexDirection}
  ${justifyContent}
`


const render = ({ is, ...props }) => {
  const Component = is ? Flex.withComponent(is) : Flex
  return <Component {...props} />
}


render.propTypes = {
  ...alignItems.propTypes,
  ...flexWrap.propTypes,
  ...flexDirection.propTypes,
  ...justifyContent.propTypes,
}


export default render
