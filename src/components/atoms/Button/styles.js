import styled from "styled-components"
import {
  height,
  width,
  space,
  variant,
  textAlign,
} from "styled-system"


const buttonStyle = variant({
  key: "buttons",
})

const Button = styled.button`
  align-items: center;
  border-radius: 5px;
  display: inline-flex;
  font-size: 14px;
  font-weight: 400;
  height: 40px;
  justify-content: center;
  margin: ${({ margin }) => (margin || "0")};
  padding: ${({ padding }) => (padding || "0 25px")};
  text-decoration: none;
  position: relative;
  white-space: nowrap;
  width: ${({ fullWidth }) => (fullWidth === true ? "100%" : "auto")};
  box-sizing: border-box;

  border-style: solid;
  border-width: 1px;
  ${buttonStyle}
  ${height}
  ${width}
  ${space}
  ${textAlign}

  &:disabled {
    opacity: .4;
    cursor: not-allowed;
  }

  &:not(:disabled) {
    cursor: pointer;
  }
`

export default Button
