import React from "react"
import styled from "styled-components"

import {
  style,
  color,
  space,
  display,
  themeGet,
  fontSize,
  textAlign,
  textStyle,
  fontWeight,
  lineHeight,
  letterSpacing,
} from "styled-system"


const whiteSpace = style({
  prop: "whiteSpace",
  cssProperty: "whiteSpace",
})


const textDecoration = style({
  prop: "textDecoration",
  cssProperty: "textDecoration",
})


const Text = styled.p`
  font-size: ${themeGet("fontSizes.1", "16")}px;
  margin: 0;
  text-overflow: ${({ textOverflow }) => (textOverflow || "")};
  text-transform: ${({ textTransform }) => (textTransform || "")};
  ${display}
  ${textStyle}
  ${color}
  ${space}
  ${fontSize}
  ${fontWeight}
  ${letterSpacing}
  ${lineHeight}
  ${textAlign}
  ${whiteSpace}
  ${textDecoration}
`


const render = ({ is, ...props }) => {
  const Component = is ? Text.withComponent(is) : Text
  return <Component {...props} />
}


export default render
