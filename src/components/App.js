import React from "react"
import { BrowserRouter } from "react-router-dom"
import { ThemeProvider } from "styled-components"

import MainPage from "/pages/Main"
import DefaultTheme from "/styles/themes"

const App = () => (
  <BrowserRouter>
    <ThemeProvider theme={DefaultTheme}>
      <MainPage/>
    </ThemeProvider>
  </BrowserRouter>
)


export default App
