import React from "react"
import ReactDOM from "react-dom"
import GlobalStyles from "./styles"

/* global document */
const mountPoint = document.querySelector("#app-root")

const render = () => {
  import("./components/App")
    .then(module => ReactDOM.render(
      <React.Fragment>
        <GlobalStyles/>
        <GlobalStyles.fonts/>
        <module.default />
      </React.Fragment>,
      mountPoint,
    ))
}

render()


if (module.hot) module.hot.accept("./components/App", render)
