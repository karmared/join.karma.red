import React from "react"
import { Redirect } from "react-router"
import { renderRoutes } from "react-router-config"

import { dynamic } from "/utils"
import { QueryRenderer, UtmWatcher } from "/components"
import { LocaleProvider, RelayEnvironmentProvider } from "/context"

/* eslint-disable comma-dangle */
const Investor = dynamic(() => import(
  /* webpackChunkName: "investor-root" */
  "./investor"
))

/* global graphql window */
const query = graphql`
  query MainPageQuery {
    viewer {
      id
      email
      country
      profiles {
        __typename
        id
        name
        ... on IndividualProfile {
          phone
        }
        ... on LegalEntityProfile {
          phone
        }
      }
      foreignProfiles {
        __typename
        id
        firstName
        lastName
        phone
      }
    }
  }
`

const routes = [
  {
    path: "/investor",
    component: Investor,
  },
  {
    component: () => <Redirect to={`/investor${window.location.search}`}/>,
  },
]

const render = props => (
  <>
    <UtmWatcher/>
    {renderRoutes(routes, props)}
  </>
)

const Main = props => (
  <RelayEnvironmentProvider>
    <LocaleProvider>
      <QueryRenderer
        {...props}
        query={query}
        render={render}
      />
    </LocaleProvider>
  </RelayEnvironmentProvider>
)

export default Main
