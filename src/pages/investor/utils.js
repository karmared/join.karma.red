export const getInvestorProfile = (viewer) => {
  if (!viewer) return null

  const profiles = [...viewer.profiles, ...viewer.foreignProfiles]
  const investor = profiles.find(profile => (
    ["IndividualProfile", "EntrepreneurProfile", "ForeignIndividualProfile"].includes(profile.__typename) // eslint-disable-line
  ))

  return investor || null
}
