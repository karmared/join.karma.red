import React from "react"

import {
  Box,
  Flex,
  Input,
  Button,
  SignupStepTitle,
} from "/components"

import { useInput } from "/hooks"

import { getInvestorProfile } from "../utils"
import { HeaderContainer } from "../Welcome/styles"

/* global window */
const Phone = (props) => {
  const { history } = props

  const phone = useInput("")

  const onFocus = (e) => {
    const value = e.target.value.trim()

    if (value[0] !== "+") {
      e.target.value = `+${e.target.value.trim()}`
    }
  }

  const onBlur = (e) => {
    const value = e.target.value.trim()

    if (value === "+") {
      e.target.value = ""
    }

    if (value && value[0] !== "+") {
      e.target.value = `+${value}`
    }
  }

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      props.setProfileData({ ...props.profileData, phone: phone.value })
      history.push(`/investor/step-5${props.location.search}`)
    },
    [phone.value],
  )

  return (
    <form onSubmit={onSubmit}>
      <HeaderContainer is={SignupStepTitle}>
        Давайте уточним ваш телефон. Вы будете использовать его для подписания документов:
      </HeaderContainer>
      <Flex
        mx="auto"
        mb="40px"
        width="230px"
        justifyContent="center"
      >
        <Input
          required
          name="phone"
          type="text"
          value={phone.value}
          placeholder="+79999999999"
          onChange={phone.onChange}
          onFocus={onFocus}
          onBlur={onBlur}
        />
      </Flex>
      <Box
        is={Button}
        m="0 auto 30px auto"
        display="block"
      >
        Готово
      </Box>
    </form>
  )
}

export default (props) => {
  if (!props.profileData || (!props.profileData.firstName && !props.profileData.name)) {
    props.history.push(`/investor/step-3${props.location.search}`)
    return null
  }

  if (getInvestorProfile(props.viewer)) {
    props.history.push(`/investor/deposit${props.location.search}`)
    return null
  }

  return <Phone {...props}/>
}
