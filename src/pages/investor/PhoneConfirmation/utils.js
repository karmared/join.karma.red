import React from "react"

import { AddIndividualProfile, CreateForeignIndividualProfile } from "/mutations"

const getRussianInvestorInput = (profileData) => {
  const input = {
    name: profileData.name,
    phone: profileData.phone,
  }

  return { input }
}

const getForeignInvestorInput = (profileData) => {
  const input = {
    attributes: {
      firstName: profileData.firstName,
      lastName: profileData.lastName,
      phone: profileData.phone,
    },
    shouldSkipValidation: true,
  }

  return { input }
}

// eslint-disable-next-line
export const getMutationAndVariables = (viewer, profileData) => {
  return viewer.country === "RU"
    ? [AddIndividualProfile, getRussianInvestorInput(profileData)]
    : [CreateForeignIndividualProfile, getForeignInvestorInput(profileData)]
}

/* global window */
export const useCountDown = (timeToCount = 60 * 1000, interval = 1000) => {
  const [secondsLeft, setSecondsLeft] = React.useState(0)
  const start = React.useCallback(() => setSecondsLeft(timeToCount / 1000), [])

  React.useEffect(
    () => {
      if (secondsLeft === 0) {
        return
      }

      window.setTimeout(() => {
        const nextSecondsLeft = secondsLeft - 1 > 0 ? secondsLeft - 1 : 0
        setSecondsLeft(nextSecondsLeft)
      }, interval)
    },
    [secondsLeft],
  )

  return [secondsLeft, start]
}
