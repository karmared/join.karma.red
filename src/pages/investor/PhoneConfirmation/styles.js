import styled from "styled-components"

export const ResendLabel = styled.span`
  margin-right: 15px;
  font-size: 14px;
  font-family: "Open Sans";
  line-height: 1.55;
`

export const ResendLink = styled.a`
  color: ${({ theme }) => theme.colors.blue};
  font-size: 14px;
  font-family: "Open Sans";
  line-height: 1.55;
  text-decoration: none;
  cursor: pointer;
  user-select: none;
  
  &:active, &:visited {
    color: ${({ theme }) => theme.colors.blue};
  }
`
