import React from "react"

import {
  Box,
  Flex,
  Input,
  Button,
  SignupStepTitle,
} from "/components"

import {
  ConfirmPhone,
  RequestPhoneConfirmation,
} from "/mutations"

import { createChainedFunction } from "/utils"
import { MutationContainer } from "/containers"
import { useInput, useEnvironment } from "/hooks"

import { getInvestorProfile } from "../utils"
import { ResendLabel, ResendLink } from "./styles"
import { HeaderContainer } from "../Welcome/styles"
import { getMutationAndVariables, useCountDown } from "./utils"

/* global window */
const EmailConfirmation = (props) => {
  const { environment } = useEnvironment()
  const clearError = path => () => props.clearError(path)
  const { history, commit } = props

  const [busy, setBusy] = React.useState(false)

  const token = useInput("")
  token.onChange = createChainedFunction(
    clearError("confirmPhone.token"),
    token.onChange,
  )

  const [secondsLeft, startCounter] = useCountDown()

  const requestPhoneConfirmation = () => {
    if (secondsLeft) {
      return
    }

    const variables = {
      input: {
        phone: props.profileData.phone,
      },
    }

    startCounter()
    RequestPhoneConfirmation.commit(
      environment,
      variables,
      null,
      {},
    )
  }

  React.useEffect(() => {
    requestPhoneConfirmation()
  }, [])

  const notifyGTM = React.useMemo(() => props.createGTMNotifier({
    eventAction: "phone-approved",
    eventLabel: "step-5",
  }))

  const onCompleted = (data) => {
    notifyGTM()
    const { profile } = data.addIndividualProfile || data.createForeignIndividualProfile
    props.setProfileData({ ...profile })

    setBusy(false)
    history.push(`/investor/deposit${props.location.search}`)
  }

  const onError = () => {
    setBusy(false)
  }

  const onTokenCompleted = () => {
    const [mutation, variables] = getMutationAndVariables(props.viewer, props.profileData)

    mutation.commit(
      environment,
      variables,
      null,
      {
        onCompleted,
        onError,
      },
    )
  }

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      const callbacks = { onCompleted: onTokenCompleted, onError }
      const variables = {
        input: {
          token: token.value,
          phone: props.profileData.phone,
        },
      }

      setBusy(true)
      commit({ environment, variables, callbacks })
    },
    [token.value],
  )

  return (
    <form onSubmit={onSubmit}>
      <HeaderContainer is={SignupStepTitle}>
        Мы близки к финишу ⭐ Введите код из смс отправленный на номер {props.profileData.phone}
      </HeaderContainer>
      <Box
        mx="auto"
        mb="10px"
        width="230px"
      >
        <Input
          required
          name="token"
          type="text"
          disabled={busy}
          value={token.value}
          placeholder="Введите код"
          onChange={token.onChange}
          error={props.getError("confirmPhone.token")}
        />
      </Box>
      <Flex
        mx="auto"
        mb="40px"
        width="fit-content"
        justifyContent="space-between"
      >
        <ResendLabel>
          Не пришел код?
        </ResendLabel>
        <ResendLink onClick={requestPhoneConfirmation}>
          Отправить еще раз
          {
            !!secondsLeft && ` через ${secondsLeft}`
          }
        </ResendLink>
      </Flex>
      <Box
        is={Button}
        disabled={busy}
        m="0 auto 30px auto"
        display="block"
      >
        Готово
      </Box>
    </form>
  )
}

export default (props) => {
  if (!props.profileData || !props.profileData.phone) {
    props.history.push(`/investor/step-4${props.location.search}`)
    return null
  }

  if (getInvestorProfile(props.viewer)) {
    props.history.push(`/investor/deposit${props.location.search}`)
    return null
  }

  return (
    <MutationContainer mutation={ConfirmPhone}>
      {
        ({ commit, getError, clearError }) => (
          <EmailConfirmation
            {...props}
            commit={commit}
            getError={getError}
            clearError={clearError}
          />
        )
      }
    </MutationContainer>
  )
}
