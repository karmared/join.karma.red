import React from "react"

import {
  Box,
  Input,
  Button,
  SignupStepTitle,
} from "/components"

import { LoginWithEmailAndPassword } from "/mutations"
import { createChainedFunction } from "/utils"
import { MutationContainer } from "/containers"
import { useInput, useEnvironment } from "/hooks"

import { getInvestorProfile } from "../utils"
import { HeaderContainer } from "../Welcome/styles"

/* global window */
const EmailConfirmation = (props) => {
  const { environment, refreshEnvironment } = useEnvironment()
  const clearError = path => () => props.clearError(path)
  const { history, commit, viewer } = props
  const emailFromStorage = window.localStorage.getItem("karma-user-email")

  const [busy, setBusy] = React.useState(false)

  const token = useInput("")
  token.onChange = createChainedFunction(
    clearError("loginWithEmailAndPassword.email"),
    clearError("loginWithEmailAndPassword.password"),
    token.onChange,
  )

  const notifyGTM = React.useMemo(() => props.createGTMNotifier({ eventAction: "code-approved", eventLabel: "step-2" }))

  const onCompleted = (data) => {
    notifyGTM()
    window.localStorage.setItem("karma-auth-token", data.loginWithEmailAndPassword.token)
    window.localStorage.removeItem("karma-user-email")
    setBusy(false)
    refreshEnvironment()
    history.push(`/investor/step-3${props.location.search}`)
  }

  const onError = () => {
    setBusy(false)
  }

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      const callbacks = { onCompleted, onError }
      const variables = {
        input: {
          email: emailFromStorage || viewer.email,
          password: token.value,
        },
      }

      setBusy(true)
      commit({ environment, variables, callbacks })
    },
    [token.value],
  )

  return (
    <form onSubmit={onSubmit}>
      <HeaderContainer is={SignupStepTitle}>
        Введите код, который мы отправили вам на почту {viewer ? viewer.email : emailFromStorage }
      </HeaderContainer>
      <Box
        mx="auto"
        mb="40px"
        width="230px"
        justifyContent="center"
      >
        <Input
          required
          name="token"
          type="password"
          disabled={busy}
          value={token.value}
          placeholder="Введите код"
          onChange={token.onChange}
          error={
            props.getError("loginWithEmailAndPassword.password")
            || props.getError("loginWithEmailAndPassword.email")
          }
        />
      </Box>
      <Box
        is={Button}
        disabled={busy}
        m="0 auto 30px auto"
        display="block"
      >
        Готово
      </Box>
    </form>
  )
}

export default (props) => {
  const email = window.localStorage.getItem("karma-user-email")
  if (!props.viewer && !email) {
    props.history.push(`/investor/step-1${props.location.search}`)
    return null
  }

  if (getInvestorProfile(props.viewer)) {
    props.history.push(`/investor/deposit${props.location.search}`)
    return null
  }

  return (
    <MutationContainer mutation={LoginWithEmailAndPassword}>
      {
        ({ commit, getError, clearError }) => (
          <EmailConfirmation
            {...props}
            commit={commit}
            getError={getError}
            clearError={clearError}
          />
        )
      }
    </MutationContainer>
  )
}
