import styled from "styled-components"
import { Flex } from "/components"

export const InputsContainer = styled(Flex)`
  justify-content: center;
  flex-wrap: wrap;
  margin-bottom: 20px;
  
  > * {
    width: 230px;
    margin-right: 15px;
    
    &:last-child {
      margin-right: 0;
    }
  }
  
  @media (max-width: 959px) {
    > * {
      width: 200px;
    }
  }
  
  @media (max-width: 639px) {
    > * {
      width: 100%;
      max-width: 300px;
      margin-bottom: 15px;
      margin-right: 0;
    }
  }
`
