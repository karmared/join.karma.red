import React from "react"

import {
  Box,
  Input,
  Button, SignupStepTitle,
} from "/components"

import { useInput } from "/hooks"

import { InputsContainer } from "./styles"
import { getInvestorProfile } from "../utils"
import { HeaderContainer } from "../Welcome/styles"

/* global window */
const Name = (props) => {
  const { history, viewer } = props

  const firstName = useInput("")
  const lastName = useInput("")
  const patronymic = useInput("")

  const notifyGTM = React.useMemo(() => props.createGTMNotifier({ eventAction: "next", eventLabel: "step-3" }))

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      notifyGTM()
      // eslint-disable-next-line
      viewer.country === "RU"
        ? props.setProfileData({
          ...props.profileData,
          name: [lastName.value, firstName.value, patronymic.value].filter(Boolean).join(" "),
        })
        : props.setProfileData({
          ...props.profileData,
          firstName: firstName.value,
          lastName: lastName.value,
        })

      history.push(`/investor/step-4${props.location.search}`)
    },
    [firstName.value, lastName.value, patronymic.value],
  )

  return (
    <form onSubmit={onSubmit}>
      <HeaderContainer is={SignupStepTitle}>
        Отлично, сейчас мы создадим аккаунт на платформе Карма, для этого введите ваши данные!
      </HeaderContainer>
      <InputsContainer>
        <Box>

          <Input
            required
            name="lastName"
            type="text"
            value={lastName.value}
            placeholder="Фамилия"
            onChange={lastName.onChange}
          />
        </Box>
        <Box>
          <Input
            required
            name="firstName"
            type="text"
            value={firstName.value}
            placeholder="Имя"
            onChange={firstName.onChange}
          />
        </Box>
        <Box>
          <Input
            name="patronymic"
            type="text"
            value={patronymic.value}
            placeholder="Отчество"
            onChange={patronymic.onChange}
          />
        </Box>
      </InputsContainer>
      <Box
        is={Button}
        m="0 auto 30px auto"
        display="block"
      >
        Далее
      </Box>
    </form>
  )
}

export default (props) => {
  if (!props.viewer) {
    props.history.push(`/investor/step-2${props.location.search}`)
    return null
  }

  if (getInvestorProfile(props.viewer)) {
    props.history.push(`/investor/deposit${props.location.search}`)
    return null
  }

  return <Name {...props}/>
}
