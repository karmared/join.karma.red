import React from "react"
import { fetchQuery } from "relay-runtime"

import { useEnvironment } from "/hooks"
import {
  Box,
  Flex,
  Button,
  SignupStepTitle,
} from "/components"

import { HeaderContainer } from "../Welcome/styles"
import {
  Social,
  AppStore,
  Subheader,
  StyledLink,
  GooglePlay,
  Background,
  AppContainer,
} from "./styles"

/* global window */
const Success = (props) => {
  const { refreshEnvironment } = useEnvironment()

  React.useEffect(() => {
    if (props.viewer) {
      window.localStorage.removeItem("karma-auth-token")
      refreshEnvironment()
    }
  }, [])

  return (
    <>
      <HeaderContainer is={SignupStepTitle}>
        Было приятно познакомиться!<br/>
        Спасибо, что выбрали Карму!
      </HeaderContainer>
      <Subheader>
        Если у Вас вдруг возникнут сложности или вопросы, вы всегда можете обратиться ко мне или к моим коллегам
      </Subheader>
      <StyledLink target="_blank" href="https://my.karma.red">
        <Box
          is={Button}
          m="0 auto 30px auto"
          display="block"
        >
          Перейти на платформу
        </Box>
      </StyledLink>
      <Background>
        <Social icon="tg" href="https://t.me/tatiana_it">Мой телеграм</Social>
        <Social icon="tg" href="https://t.me/karmainvest">Чат для<br/>инвесторов</Social>
        <Social icon="email" href="mailto:help@karma.red">help@karma.red</Social>
        <Social icon="web" href="https://my.karma.red">my.karma.red</Social>
      </Background>
      <Box my="40px">
        <Subheader>
          Мобильные приложения
        </Subheader>
      </Box>
      <Flex width="150px" mx="auto" my="40px" justifyContent="space-between">
        <AppContainer href="https://itunes.apple.com/us/app/karma-investments/id1451475987">
          <AppStore/>
        </AppContainer>
        <AppContainer href="https://play.google.com/store/apps/details?id=ru.karma.android">
          <GooglePlay/>
        </AppContainer>
      </Flex>
    </>
  )
}

export default Success
