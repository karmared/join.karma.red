import styled from "styled-components"

import { Box, Flex } from "/components"

export const InputsContainer = styled(Flex)`
  justify-content: center;
  flex-wrap: wrap;
  margin-bottom: 20px;
  
  > * {
    width: 230px;
    
    &:first-child {
      margin-right: 15px;
    }
  }
  
  @media (max-width: 479px) {
    > * {
      width: 100%;
      max-width: 260px;
      margin-bottom: 15px;
      
      &:first-child {
        margin-right: 0;
      }
    }
  }
`

export const HeaderContainer = styled(Box)`
  padding: 20px;
  margin: 10px auto 40px auto !important;
  width: 850px;
  max-width: 100%;
  font-size: 25px !important;
`

export const CheckBoxesContainer = styled(Flex)`
  justify-content: space-between;
  flex-wrap: wrap;
  max-width: 1100px;
  
  @media (max-width: 959px) {
    flex-direction: column;
    align-items: center;
    
    > * {
      margin-bottom: 15px;
    }
  }
  
  @media (max-width: 479px) {
    > * {
      width: 290px;
    }
  }
`

export const StyledLink = styled.a`
  color: inherit;
  
  &:active, &:visited {
    color: inherit;
  }
`
