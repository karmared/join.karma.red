import React from "react"
import { fetchQuery } from "relay-runtime"

import {
  Box,
  Input,
  Button,
  Select,
  CheckBox,
  SignupStepTitle,
} from "/components"

import {
  pipe,
  getCid,
  getUtmContent,
  createChainedFunction,
} from "/utils"

import { RegisterUser } from "/mutations"
import { MutationContainer } from "/containers"
import { REGISTRATION_SOURCE } from "/constants"
import { useInput, useEnvironment } from "/hooks"

import {
  StyledLink,
  InputsContainer,
  HeaderContainer,
  CheckBoxesContainer,
} from "./styles"

/* global graphql window */
const welcomePageQuery = graphql`
  query WelcomePageQuery {
    countries {
      code
      name
    }
  }
`

const isEmailUniquenessError = error => (
  error && error.props.i18n === "registerUser.email.unique"
)

const decorateEmailUniquenessError = (error) => {
  if (!error) return null

  return (
    isEmailUniquenessError(error)
      ? (
        <Box>
          {error}.
          &nbsp;
          <StyledLink href="https://my.karma.red/login">
            Войти
          </StyledLink>
        </Box>
      )
      : error
  )
}

const Welcome = (props) => {
  const { environment, refreshEnvironment } = useEnvironment()
  const { viewer, history, commit } = props

  const [busy, setBusy] = React.useState(false)
  const termsOfUse = useInput(false)
  const personalDataAgreement = useInput(false)
  const shouldSubscribeToMailingList = useInput(false)

  const clearError = path => () => props.clearError(path)

  const email = useInput(viewer ? viewer.email : "")
  email.onChange = createChainedFunction(clearError("registerUser.email"), email.onChange)

  const country = useInput(viewer ? viewer.country : "RU")
  country.onChange = createChainedFunction(clearError("registerUser.country"), country.onChange)

  const notifyGTM = React.useMemo(() => props.createGTMNotifier({ eventAction: "next", eventLabel: "step-1" }))

  const [countries, setCountries] = React.useState([])
  React.useEffect(() => {
    fetchQuery(
      environment,
      welcomePageQuery,
      {},
    ).then((data) => {
      setCountries(
        data.countries.map(item => ({ value: item.code, label: item.name })),
      )
    })
  }, [])

  const onCompleted = () => {
    setBusy(false)
    window.localStorage.removeItem("karma-auth-token")
    window.localStorage.setItem("karma-user-email", email.value)
    history.push(`/investor/step-2${props.location.search}`)
    refreshEnvironment()
  }

  const onError = () => {
    setBusy(false)
  }

  const onSubmit = React.useCallback(
    (e) => {
      e.preventDefault()

      notifyGTM()

      if (viewer && email.value === viewer.email) {
        window.localStorage.setItem("karma-user-email", email.value)
        history.push(`/investor/step-2${props.location.search}`)
        return
      }

      const callbacks = { onCompleted, onError }
      const variables = {
        input: {
          email: email.value,
          country: country.value,
          shouldSubscribeToMailingList: shouldSubscribeToMailingList.value,
          source: `${REGISTRATION_SOURCE}-${props.order.chain.id.split(".")[2]}`,
          analytics: {
            cid: getCid(),
            utms: getUtmContent(),
            userAgent: window.navigator ? window.navigator.userAgent : null,
          },
        },
      }

      setBusy(true)
      commit({ environment, variables, callbacks })
    },
    [email.value, viewer, country.value],
  )

  const getEmailError = pipe(
    () => props.getError("registerUser.email"),
    decorateEmailUniquenessError,
  )

  return (
    <>
      <HeaderContainer is={SignupStepTitle}>
        Привет, я — Таня! Я помогу вам зарегистрироваться.
        Давайте уточним вашу почту.
        Вы будете использовать ее для входа в систему.
      </HeaderContainer>
      <form onSubmit={onSubmit}>
        <InputsContainer>
          <Box>
            <Input
              required
              name="email"
              type="email"
              disabled={busy}
              value={email.value}
              placeholder="Email"
              onChange={email.onChange}
              error={getEmailError()}
            />
          </Box>
          <Box>
            <Select
              required
              disabled={busy}
              options={countries}
              value={country.value}
              onChange={country.onChange}
            />
          </Box>
        </InputsContainer>
        <Box
          is={Button}
          disabled={busy}
          m="0 auto 30px auto"
          display="block"
        >
          Далее
        </Box>
        <CheckBoxesContainer>
          <Box width="320px">
            <CheckBox
              required
              checked={termsOfUse.value}
              onChange={termsOfUse.onChange}
            >
              Принимаю условия пользования сервисом
            </CheckBox>
          </Box>
          <Box width="320px">
            <CheckBox
              required
              checked={personalDataAgreement.value}
              onChange={personalDataAgreement.onChange}
            >
              Даю согласие на хранение и обработку моих персональных данных
            </CheckBox>
          </Box>
          <Box width="320px">
            <CheckBox
              checked={shouldSubscribeToMailingList.value}
              onChange={shouldSubscribeToMailingList.onChange}
            >
              Согласен на получение информации о состоянии счета, всех проводимых операциях и выгодных предложениях
            </CheckBox>
          </Box>
        </CheckBoxesContainer>
      </form>
    </>
  )
}

export default props => (
  <MutationContainer mutation={RegisterUser}>
    {
      ({ commit, getError, clearError }) => (
        <Welcome
          {...props}
          commit={commit}
          getError={getError}
          clearError={clearError}
        />
      )
    }
  </MutationContainer>
)
