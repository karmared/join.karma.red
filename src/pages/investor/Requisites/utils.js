const getRequisiteValue = item => (
  item.key === "correspondentAccount"
    ? item.value.split(/\s/i)[0]
    : item.value
)

export const buildQRCodeString = (requisites) => {
  const propertiesToInclude = {
    receiverName: ["Name", 0],
    nominalAccount: ["PersonalAcc", 1],
    bankName: ["BankName", 2],
    bankBIC: ["BIC", 3],
    correspondentAccount: ["CorrespAcc", 4],
    receiverTin: ["PayeeINN", 5],
    receiverRCR: ["KPP", 6],
    purpose: ["Purpose", 7],
  }

  const tokens = requisites.reduce((memo, item) => {
    if (!propertiesToInclude[item.key]) return memo

    const [key, position] = propertiesToInclude[item.key]
    memo[position] = `${key}=${getRequisiteValue(item)}` // eslint-disable-line

    return [...memo]
  }, [])

  return ["ST00012", ...tokens].join("|")
}
