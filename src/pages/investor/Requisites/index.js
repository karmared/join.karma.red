import React from "react"
import QRCode from "qrcode.react"
import { fetchQuery } from "relay-runtime"

import { useEnvironment } from "/hooks"
import { MailInvestorPayment } from "/mutations"
import {
  Box,
  Text,
  Button,
  Loader,
  SignupStepTitle,
} from "/components"

import { HeaderContainer } from "../Welcome/styles"
import {
  InfoText,
  Subheader,
  DangerText,
  Instructions,
  InfoContainer,
  InstructionsLink,
  BankRequisitesItem,
  RequisitesContainer,
  BackgroundContainer,
} from "./styles"

import { buildQRCodeString } from "./utils"
import { getInvestorProfile } from "../utils"

/* global graphql window */
const query = graphql`
  query RequisitesQuery($orderId: ID!, $profileId: ID!) {
    node(id: $orderId) {
      ... on Order {
        id
        application {
          shortTitle
        }
      }
    }
    
    localizedStrings {
      karmaPaymentRequisites {
        name
        value
        key
      }
      investorPaymentPurpose(profileId: $profileId, orderId: $orderId) {
        name
        value
        key
      }
    }
  }
`

const Requisites = (props) => {
  const { environment } = useEnvironment()
  const [order, setOrder] = React.useState(null)
  const [requisites, setRequisites] = React.useState(null)
  const [requisitesString, setRequisitesString] = React.useState(null)

  React.useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search)
    const investor = getInvestorProfile(props.viewer)

    const variables = {
      orderId: urlParams.get("order"),
      profileId: investor.id,
    }

    fetchQuery(environment, query, variables).then((data) => {
      setOrder(data.node)
      setRequisites(data.localizedStrings)

      const reqStr = buildQRCodeString([
        ...data.localizedStrings.karmaPaymentRequisites,
        ...data.localizedStrings.investorPaymentPurpose,
      ])

      setRequisitesString(reqStr)
    })

    const mailVariables = {
      input: { orderId: urlParams.get("order") },
    }

    MailInvestorPayment.commit(
      environment,
      mailVariables,
      null,
      {},
    )
  }, [])

  if (!order || !requisites) {
    return (
      <Box mt="200px">
        <Loader fontSize="14px"/>
      </Box>
    )
  }

  return (
    <>
      <HeaderContainer is={SignupStepTitle}>
        Ура, вы зарегистрировались ❤️<br/><br/>
        Пополните свой счет на платформе Карма, чтобы инвестировать в заявку {order.application.shortTitle}:
      </HeaderContainer>
      <RequisitesContainer>
        <BackgroundContainer>
          {
            requisites.karmaPaymentRequisites.map(({ name, value }, idx) => (
              <BankRequisitesItem key={idx}>{name}: {value}</BankRequisitesItem>
            ))
          }
          {
            requisites.investorPaymentPurpose.map(({ name, value }, idx) => (
              <BankRequisitesItem key={idx}>{name}: {value}</BankRequisitesItem>
            ))
          }
            <DangerText>
              Внимание! Минимальный платеж: 5000 р.<br/>
              ФИО отправителя средств должно соответствовать ФИО владельца счета в Карме.
            </DangerText>
        </BackgroundContainer>
        <Box>
          <Subheader>
            Банковским переводом с помощью QR-кода
          </Subheader>
          <Box width="fit-content" m="auto" mb="10px">
            {
              requisitesString && (
                <QRCode value={requisitesString} level="H" size={256}/>
              )
            }
          </Box>
          <Text textAlign="center">
            <InstructionsLink href="#instructions">
              Инструкция для Сбербанк и Тинькофф
            </InstructionsLink>
          </Text>
        </Box>
      </RequisitesContainer>

      <Box
        is={Button}
        m="20px auto 30px auto"
        display="block"
        onClick={() => props.history.push(`/investor/success${props.location.search}`)}
      >
        Далее
      </Box>
      <InfoContainer>
        <InfoText>* Для удобства мы продублировали вам реквизиты на почту</InfoText>
        <InfoText>* Средний срок зачисления средств 8 рабочих часов.</InfoText>
        <InfoText>
          * Как только ваши средства поступят на платформу, мы вас об этом оповестим через SMS и электронную почту.
        </InfoText>
      </InfoContainer>
      <Instructions/>
    </>
  )
}

export default (props) => {
  if (!getInvestorProfile(props.viewer)) {
    props.history.push(`/investor/step-5${props.location.search}`)
    return null
  }

  return <Requisites {...props}/>
}
