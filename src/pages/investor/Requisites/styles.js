import React from "react"
import styled from "styled-components"

import { Box, Flex, Text } from "/components"

export const Subheader = styled(Text)`
  text-align: ${({ textAlign }) => (textAlign || "center")};
  margin-bottom: 10px;
  font-size: 18px;
  font-family: "Open Sans";
  line-height: 1.55;
  font-weight: 600;
`

export const Header = styled(Text)`
  text-align: center;
  font-size: 24px;
  font-family: "Open Sans";
  line-height: 1.55;
  font-weight: 600;
  padding: 0 10px;
`

const Background = styled.div`
  width: 564px;
  max-width: 100%;
  padding: 50px;
  text-align: left;
  background-color: ${({ theme }) => theme.colors.greyLight};
`

export const BgContainer = styled.div`
  max-width: 100%;
`

export const BackgroundContainer = props => (
  <BgContainer>
    <Subheader>Банковским переводом вручную</Subheader>
    <Background {...props}/>
  </BgContainer>
)

export const BankRequisitesItem = styled(Text)`
  color: ${({ theme }) => theme.colors.greyShuttle};
  font-size: 16px;
  font-family: "Open Sans";
  line-height: 1.55;
  font-weight: 400;
`

export const DangerText = styled(Text)`
  margin-top: 15px;
  color: ${({ theme }) => theme.colors.red};
  font-size: 14px;
  font-family: "Open Sans";
  line-height: 1.55;
  font-weight: bold;
`

export const InfoContainer = styled.div`
  width: 564px;
  max-width: 100%;
  margin: auto;
  margin-bottom: 40px;
`

export const InfoText = styled(Text)`
  padding-left: 10px;
  color: ${({ theme }) => theme.colors.greyShuttle};
  font-size: 14px;
  font-family: "Open Sans";
  line-height: 1.8;
`

export const RequisitesContainer = styled(Flex)`
  justify-content: space-around;
  
  @media (max-width: 959px) {
    flex-direction: column;
    
    > * {
      margin: auto;
      margin-bottom: 30px;
    }
  }
`

const ListItem = styled.li`
  margin-bottom: 10px;
  font-size: 12px;
  font-family: "Open Sans";
  
  &:before {
    counter-increment: list;
    content: counter(list);
    display: inline-block;
    padding: 3px;
    font-size: 12px;
    line-height: 1;
    vertical-align: middle;
    text-align: center;
    width: 20px;
    height: 20px;
    background-color: ${({ color }) => color || "#f6ce50"};
    border-radius: 50%;
    margin-right: 10px;
  }
`

const List = styled.ol`
  list-style: none;
  counter-reset: list;
  margin: 0;
  padding: 0;
`

export const InstructionContainer = styled(Flex)`
  @media (max-width: 479px) {
    flex-direction: column;
    
    > * {
      margin: auto !important;
      margin-bottom: 20px !important;
    }
  }
`

export const Instruction = ({
  src,
  title,
  items,
  color,
}) => (
  <InstructionContainer>
    <Box
      is="img"
      width="200px"
      height="410px"
      mr="40px"
      src={src}
    />
    <Box>
      <Box is={Subheader} mb="20px" textAlign="left">
        {title}
      </Box>
      <List>
        {
          items.map((item, idx) => (
            <ListItem key={idx} color={color}>{item}</ListItem>
          ))
        }
      </List>
    </Box>
  </InstructionContainer>
)

const InstructionsContainer = styled(Flex)`
  padding: 0 15px;
  margin-top: 50px;
  
  @media (max-width: 959px) {
    flex-direction: column;
    
    > * {
      margin: auto;
      margin-bottom: 40px;
    }
  }
`

export const Instructions = () => (
  <Box my="70px">
    <Header id="instructions">
      Как сделать перевод с помощью QR-кода
    </Header>
    <InstructionsContainer>
      <Instruction
        src="/images/qr_tinkoff.png"
        title="Тинькофф"
        items={[
          "Зайдите в раздел \"оплатить\" в нижнем меню",
          "Выберите \"организациям\"",
          "Выберите \"юр. лицу\"",
          "Выберите \"Сканировать кватанцию\"",
          "Отсканируйте QR-код, введите сумму и подтвердите перевод",
        ]}
      />
      <Instruction
        src="/images/qr_sber.png"
        title="Сбербанк"
        color="#0ca94e"
        items={[
          "Выберите счет списания",
          "Выберите \"платеж или перевод\"",
          "Выберите \"оплата по QR или штрихкоду\"",
          "Отсканируйте QR-код, введите сумму и подтвердите перевод",
        ]}
      />
    </InstructionsContainer>
  </Box>
)

export const InstructionsLink = styled.a`
  text-decoration: none;
  color: rgb(15, 103, 186);
  font-family: "Open Sans";
  
  &:visited, &:active {
    color: rgb(15, 103, 186)
  }
`
