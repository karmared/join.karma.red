import React from "react"
import styled from "styled-components"
import { fetchQuery } from "relay-runtime"

import {
  Box,
  Loader,
  SignupHeader,
} from "/components"
import { notifyGTM } from "/utils"
import { useEnvironment } from "/hooks"

import { getInvestorProfile } from "./utils"

const StyledBox = styled(Box)`
  max-width: 1200px;
  margin: auto;
  
  @media (max-width: 1199px) {
    max-width: 960px;
  }
  
  @media (max-width: 959px) {
    max-width: 640px;
  }
  
  @media (max-width: 639px) {
    max-width: 480px;
  }
  
  @media (max-width: 479px) {
    max-width: 100%;
  }
`

/* global graphql */
const query = graphql`
  query ContainerInfoQuery($id: ID!) {
    node(id: $id) {
      id
      ... on Order {
        status
        chain {
          id
        }
      }
    }
  }
`

/* global window */
export default (props) => {
  const initialProfileData = getInvestorProfile(props.viewer)
  const [profileData, setProfileData] = React.useState(initialProfileData)
  const [order, setOrder] = React.useState(null)
  const { environment } = useEnvironment()

  React.useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search)
    const orderId = urlParams.get("order")
    const variables = { id: orderId }

    fetchQuery(
      environment,
      query,
      variables,
    ).then((data) => {
      const { node } = data

      if (node.status !== "CONFIRMED") {
        throw new Error("Investment period is expired!")
      }

      setOrder(node)
    }).catch(() => {
      setOrder({})
      props.history.push(`/investor/wrong${window.location.search}`)
    })
  }, [])

  const createGTMNotifier = React.useCallback(data => notifyGTM({
    ...data,
    event: "landing-registry",
  }), [order])

  return (
    <StyledBox>
      <SignupHeader/>
      {
        order
          ? props.children({
            order,
            profileData,
            setProfileData,
            createGTMNotifier,
          })
          : (
            <Box mt="200px">
              <Loader fontSize="14px"/>
            </Box>
          )
      }
    </StyledBox>
  )
}
