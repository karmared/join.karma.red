/* global window */
import React from "react"
import { Redirect } from "react-router"
import { renderRoutes } from "react-router-config"

import Name from "./Name"
import Phone from "./Phone"
import Welcome from "./Welcome"
import Success from "./Success"
import Container from "./Container"
import WrongLink from "./WrongLink"
import Requisites from "./Requisites"
import PhoneConfirmation from "./PhoneConfirmation"
import EmailConfirmation from "./EmailConfirmation"

const routes = [
  {
    path: "/investor/step-1",
    component: Welcome,
  },
  {
    path: "/investor/step-2",
    component: EmailConfirmation,
  },
  {
    path: "/investor/step-3",
    component: Name,
  },
  {
    path: "/investor/step-4",
    component: Phone,
  },
  {
    path: "/investor/step-5",
    component: PhoneConfirmation,
  },
  {
    path: "/investor/deposit",
    component: Requisites,
  },
  {
    path: "/investor/success",
    component: Success,
  },
  {
    path: "/investor/wrong",
    component: WrongLink,
  },
  {
    component: () => <Redirect to={`/investor/step-1${window.location.search}`}/>,
  },
]

export default props => (
  <Container viewer={props.viewer} history={props.history}>
    {
      containerProps => renderRoutes(routes, { ...props, ...containerProps })
    }
  </Container>
)
