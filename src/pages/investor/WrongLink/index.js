import React from "react"

import {
  Box,
  SignupStepTitle,
} from "/components"

import { HeaderContainer } from "../Welcome/styles"

const WrongLink = () => (
  <Box>
    <HeaderContainer is={SignupStepTitle}>
      Кажется, вы используете неверную ссылку :(
      Попробуйте вернуться на страницу заявки и нажать кнопку "Инвестировать" снова.
    </HeaderContainer>
  </Box>
)

export default WrongLink
