require("../config/env")
require("isomorphic-fetch")
const fs = require("fs")
const paths = require("../config/paths")
const {
  printSchema,
  buildClientSchema,
  introspectionQuery,
} = require("graphql")


fetch(process.env.GRAPHQL_ENDPOINT, {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({ query: introspectionQuery }),
})
  .then(response => response.json())
  .then(json => {
    fs.writeFileSync(paths.schema, printSchema(buildClientSchema(json.data)))
  })
