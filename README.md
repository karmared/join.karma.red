# my.karma.red frontend


### Environment
```
GRAPHQL_ENDPOINT=https://devnet-my.karma.red/graphql
```
Consumed from `.env` file or|and environment variables. Environment variables take precedence.


### Dev environment
```
PORT=4200
```


### Install
```sh
yarn
yarn build
```


### Start
```sh
yarn serve
```
Or just use `public` folder as static site root
